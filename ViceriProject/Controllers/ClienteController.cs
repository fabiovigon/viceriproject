﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViceriProject.Models;

namespace ViceriProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private DataContext db = new DataContext();

        [Produces("application/json")]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var clientes = db.Clientes.ToList().Where(x => x.Flag_ativo);
                return Ok(clientes);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [Produces("application/json")]
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetId(int id)
        {
            try
            {
                var clientes = db.Clientes.SingleOrDefault(p => p.ID == id);
                return Ok(clientes);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        [Produces("application/json")]
        [HttpPost("Post")]
        public async Task<IActionResult> PostCliente([FromBody] Cliente cliente)
        {
            try
            {
            var obj = db.Clientes.FirstOrDefault(x => x.CNPJ == cliente.CNPJ && !x.Flag_ativo);
            if (obj != null)

            {
                obj.Flag_ativo = true;
                db.SaveChanges();
            }
            else
            {
                cliente.Flag_ativo = true;
                db.Clientes.Add(cliente);
                db.SaveChanges();
            }

            return Ok(cliente);
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateCliente([FromBody] Cliente cliente)
        {
            try
            {
                cliente.Flag_ativo = true;
                db.Entry(cliente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(cliente);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }


        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var cliente = db.Clientes.SingleOrDefault(p => p.ID == id);
                cliente.Flag_ativo = false;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}