﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViceriProject.Models;

namespace ViceriProject.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class FuncionarioController : ControllerBase
    {

        private DataContext db = new DataContext();

        [Produces("application/json")]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var funcionarios = db.Funcionarios.ToList().Where(x => x.Flag_ativo);
                return Ok(funcionarios);
            }
            catch (Exception e)
            {
                throw e;
            }


        }
        [Produces("application/json")]
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetId(int id)
        {
            try
            {
                var funcionarios = db.Funcionarios.SingleOrDefault(p => p.Id == id);
                return Ok(funcionarios);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [Produces("application/json")]
        [HttpPost("Post")]
        public async Task<IActionResult> PostFuncionario([FromBody] Funcionario funcionario)
        {
            try
            {
                var obj = db.Funcionarios.FirstOrDefault(x => x.CPF == funcionario.CPF && !x.Flag_ativo);
                if (obj != null)
                {
                    obj.Flag_ativo = true;
                    db.SaveChanges();
                }
                else
                {
                    string nome = Dns.GetHostName();
                    IPHostEntry ip = Dns.GetHostEntry(funcionario.IP_DE_QUEM_CRIOU = nome);
                    //funcionario.IP_DE_QUEM_CRIOU = ip[1].ToString(); Lista de IP
                    funcionario.Flag_ativo = true;
                    db.Funcionarios.Add(funcionario);
                    db.SaveChanges();
                }

                return Ok(funcionario);
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateFuncionario([FromBody] Funcionario funcionario)
        {
            try
            {
                string nome = Dns.GetHostName();
                IPAddress[] ip = Dns.GetHostAddresses(funcionario.IP_DE_QUEM_CRIOU = nome);
                //funcionario.IP_DE_QUEM_CRIOU = ip[5].ToString();
                funcionario.Flag_ativo = true;
                db.Entry(funcionario).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(funcionario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var funcionario = db.Funcionarios.SingleOrDefault(p => p.Id == id);
                funcionario.Flag_ativo = false;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}