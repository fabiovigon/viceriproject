﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViceriProject.Models;

namespace ViceriProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoController : ControllerBase
    {
        private DataContext db = new DataContext();

        [Produces("application/json")]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var projetos = db.Projetos.ToList();
                return Ok(projetos);
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetId(int id)
        {
            try
            {
                var projeto = db.Projetos.SingleOrDefault(p => p.ID == id);
                return Ok(projeto);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPost("Post")]
        public async Task<IActionResult> PostProjetos([FromBody] Projeto projeto)
        {
            try
            {
                var obj = db.Projetos.FirstOrDefault(x => x.NOME_PROJETO == projeto.NOME_PROJETO);
                if (obj != null)
                {

                    throw new System.ArgumentException("Error");
                }
                else
                {
                   
                    db.Projetos.Add(projeto);
                    db.SaveChanges();
                }

                return Ok(projeto);
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateProjetos([FromBody] Projeto projeto)
        {
                try
                {
                    db.Entry(projeto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                    return Ok(projeto);
                }
                catch (Exception e)
                {
                    throw e.GetBaseException();
                }
            }


        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {

            try
            {
                db.Remove(db.Projetos.Find(id));
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }
        }
    }

