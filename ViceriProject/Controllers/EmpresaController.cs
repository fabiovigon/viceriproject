﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViceriProject.Models;

namespace ViceriProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        private DataContext db = new DataContext();
        
        [Produces("application/json")]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var empresas = db.Empresas.ToList().Where(x => x.Flag_ativo);
                return Ok(empresas);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetId(int id)
        {
            try
            {
                var empresas = db.Empresas.SingleOrDefault(p => p.ID == id);
                return Ok(empresas);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPost("Post")]
        public async Task<IActionResult> PostEmpresa([FromBody] Empresa empresa)
        {
            try
            {
                var obj = db.Empresas.FirstOrDefault(x => x.CNPJ == empresa.CNPJ && !x.Flag_ativo);
                if (obj != null)

                {
                    obj.Flag_ativo = true;
                    db.SaveChanges();
                }
                else
                {
                    empresa.Flag_ativo = true;
                    db.Empresas.Add(empresa);
                    db.SaveChanges();
                }

                return Ok(empresa);
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateEmpresa([FromBody] Empresa empresa)
        {
            try
            {
                empresa.Flag_ativo = true;
                db.Entry(empresa).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(empresa);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var empresa = db.Empresas.SingleOrDefault(p => p.ID == id);
                empresa.Flag_ativo = false;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }
    }
}