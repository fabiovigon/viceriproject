﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViceriProject.Models;

namespace ViceriProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoFuncController : ControllerBase
    {
        private DataContext db = new DataContext(); 

        [Produces("application/json")]
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var projetos = db.ProjetoFunc.ToList();
                return Ok(projetos);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            } 
        }

        [Produces("application/json")]
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetId(int id)
        {
            try
            {
                var projeto = db.ProjetoFunc.SingleOrDefault(x => x.Id == id);
                return Ok(projeto);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPost("Post")]
        public async Task<IActionResult> PostProjetoFunc([FromBody] Projeto_Func projeto_Func)
        {
            try
            {
                var obj = db.ProjetoFunc.FirstOrDefault(x => x.FK_PROJETO == projeto_Func.FK_PROJETO);
                if (obj != null)
                {
                    throw new System.ArgumentException("Error");
                   
                }
                else
                {

                    db.ProjetoFunc.Add(projeto_Func);
                    db.SaveChanges();
                }
                return Ok(projeto_Func);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [Produces("application/json")]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateProjetoFunc([FromBody] Projeto_Func projeto_Func)
        {
            try
            {
                db.Entry(projeto_Func).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                return Ok(projeto_Func);
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                db.Remove(db.ProjetoFunc.Find(id));
                db.SaveChanges();
                return Ok();
            }
            catch(Exception e)
            {
                throw e.GetBaseException();
            }
        }
    }
}