﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViceriProject.Input
{
    [Table("Funcionario_infos")]
    public class FuncionarioInput
    {
        public string Nome_Func { get; set; }
        public string Cargo { get; set; }
        public string CPF { get; set; }
    }
}
