﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViceriProject.Models
{
    [Table("Cliente_infos")]
    public class Cliente
    {
        public int ID { get; set; }
        public string NOME_CLI { get; set; }
        public int ID_PROJETO_CLIENTE { get; set; }
        public string CNPJ { get; set; }
        public bool Flag_ativo { get; set; }
    }
}
