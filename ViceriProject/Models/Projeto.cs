﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViceriProject.Models
{
    [Table("Projetos")]
    public class Projeto
    {
        public int ID { get; set; }
        public string NOME_PROJETO { get; set; }
    }
}
