﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViceriProject.Models
{
    [Table("Empresa")]
    public class Empresa
    {
        public int ID { get; set; }
        public string NOME_EMPRESA { get; set; }
        public int ID_CLI { get; set; }
        public int ID_FUNC { get; set; }
        public string CNPJ { get; set; }
        public bool Flag_ativo { get; set; }
    }
}
