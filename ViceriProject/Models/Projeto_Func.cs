﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViceriProject.Models
{
    [Table("Projeto_Func")]
    public class Projeto_Func
    {
        public int Id { get; set; }
        public int FK_PROJETO { get; set; }
    }
}
