﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Domain.Entities
{
   public class Funcionario
    {
        public string Nome_Func { get; set; }
        public string Cargo { get; set; }
        public string IP_DE_QUEM_CRIOU { get; set; }
        public bool Flag_ativo { get; set; }
        public string CPF { get; set; }
        public int ID_PROJETOS_FUNC { get; set; }
    }
}
