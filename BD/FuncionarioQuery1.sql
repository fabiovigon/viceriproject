USE Funcionario
GO
--------------------------------------------------------------
---------------------------Tabelas----------------------------
--------------------------------------------------------------

CREATE TABLE Funcionario_infos
(
	[ID] INT IDENTITY NOT NULL,
	[NOME_FUNC] VARCHAR(50) NOT NULL,
	[CARGO] VARCHAR(30) NOT NULL,
	[IP_DE_QUEM_CRIOU] VARCHAR(15),
	[Flag_ativo] bit,
	[CPF] VARCHAR(11) NOT NULL UNIQUE,
	PRIMARY KEY (ID)

	
)
GO

CREATE TABLE Log_Funcionarios
(
	[ID_LOG] INT IDENTITY NOT NULL,
	[ID_USUARIO] INT NOT NULL,
	[DATA] DATETIME,
	[IPMAQUINA] VARCHAR(15) NOT NULL,
	PRIMARY KEY (ID_LOG),
	FOREIGN KEY (ID_USUARIO) REFERENCES Funcionario_infos(ID)
)
GO

CREATE TABLE Empresa
(
[ID] INT IDENTITY NOT NULL,
[NOME_EMPRESA] VARCHAR NOT NULL,
[ID_CLI] INT NOT NULL,
[ID_FUNC] INT NOT NULL,
[CNPJ] VARCHAR(18) NOT NULL UNIQUE,
PRIMARY KEY (ID),
FOREIGN KEY ([ID_CLI]) REFERENCES Cliente_infos(ID),
FOREIGN KEY ([ID_FUNC]) REFERENCES Funcionario_infos(ID)
)
GO
CREATE TABLE Cliente_infos
(
[ID] INT IDENTITY NOT NULL,
[NOME_CLI] VARCHAR(50) NOT NULL,
[ID_PROJETO_CLIENTE] INT NOT NULL,
[CNPJ] INT NOT NULL UNIQUE,
PRIMARY KEY(ID),
FOREIGN KEY ([ID_PROJETO_CLIENTE]) REFERENCES Projetos(ID)
)
GO

CREATE TABLE Projetos
(
[ID] INT IDENTITY NOT NULL,
[NOME_PROJETO] VARCHAR(50) NOT NULL,
PRIMARY KEY (ID),

)
GO

CREATE TABLE Projeto_Func
(
	[ID] INT IDENTITY NOT NULL,
	[FK_PROJETO] INT NOT NULL,
	PRIMARY KEY (ID),
	FOREIGN KEY([FK_PROJETO]) REFERENCES Projetos(ID)
)
GO


--------------------------------------------------------------
---------------------------SELECT-----------------------------
--------------------------------------------------------------

SELECT * FROM Funcionario_infos
GO

SELECT * FROM Empresa
GO

SELECT * FROM Log_Funcionarios
GO

SELECT * FROM Cliente_infos
GO

SELECT * FROM Projeto_Func
GO

SELECT * FROM Projetos
GO

SELECT * FROM  Cliente_infos
ALTER TABLE Funcionario_infos
ADD  [CPF] VARCHAR(11) NOT NULL


--------------------------------------------------------------
---------------------------ALTER------------------------------
--------------------------------------------------------------

ALTER TABLE Funcionario_infos
ADD UNIQUE (CPF);

ALTER TABLE Cliente_infos
ADD [Flag_ativo] bit NOT NULL

ALTER TABLE Funcionario_infos
ADD [ID_PROJETOS_FUNC] INT NOT NULL default 0;

ALTER TABLE Funcionario_infos
ADD  FOREIGN KEY (ID_PROJETOS_FUNC) REFERENCES Projeto_Func (ID);


--------------------------------------------------------------
---------------------------INSERT-----------------------------
--------------------------------------------------------------

INSERT INTO Funcionario_infos([NOME_FUNC],[CARGO], [IP_DE_QUEM_CRIOU],[Flag_ativo],[CPF])
VALUES ( 'F�bio', 'Desenvolvedor','192.168.0', 1,'42750347831')
GO


--------------------------------------------------------------
---------------------------UPDATE----------------------------
--------------------------------------------------------------
update Funcionario_infos set Cpf ='11122233333' WHERE ID = 12


--------------------------------------------------------------
---------------------------DELETE----------------------------
--------------------------------------------------------------

DELETE FROM Funcionario_infos WHERE ID= 2
GO



--------------------------------------------------------------
---------------------------TRIGGER----------------------------
--------------------------------------------------------------

CREATE TRIGGER [LOG_TRIGGER]
ON [Funcionario_infos]
AFTER INSERT 
AS

CREATE TABLE #Tabelinha ([ID] INT NOT NULL,
					[NOME_FUNC] VARCHAR(50) NOT NULL,
					[CARGO] VARCHAR(30) NOT NULL,	
					[IP_DE_QUEM_CRIOU] VARCHAR(15),
					[Flag_ativo] bit,
					[ID_PROJETOS_FUNC] INT NOT NULL,
					[CPF] VARCHAR(11)  NOT NULL UNIQUE
					 )

INSERT INTO #Tabelinha SELECT * FROM INSERTED

DECLARE @ID_USUARIO INT  
SET @ID_USUARIO = (SELECT ID FROM #Tabelinha )

DECLARE @IP_MAQUINA VARCHAR(15)
SET @IP_MAQUINA = (SELECT [IP_DE_QUEM_CRIOU] FROM #Tabelinha)

DECLARE @DATA DATETIME
SET @DATA = GETDATE()

INSERT INTO Log_Funcionarios values (@ID_USUARIO , @DATA, @IP_MAQUINA) 
DROP TABLE #TABELINHA
GO