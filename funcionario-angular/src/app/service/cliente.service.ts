import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../entities/cliente.entity';

@Injectable()
export class ClienteService {
  constructor(private http: HttpClient) { }
  url = 'https://localhost:44311/api/Cliente';

  getAll() {
    return this.http.get<Cliente[]>(this.url + '/' + 'GetAll');
  }

  getId(id: number) {
    return this.http.get<Cliente>(this.url + '/Get/' + id);
  }

  post(cliente: Cliente) {
    return this.http.post(this.url + '/Post', cliente);
  }

  update(cliente: Cliente) {
    return this.http.put(this.url + '/Update' , cliente );
  }

  delete(id: number) {
    return this.http.delete(this.url + '/Delete/' + id);
  }
}
