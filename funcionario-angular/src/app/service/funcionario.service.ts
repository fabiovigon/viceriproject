import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Funcionario } from '../entities/funcionario.entity';

@Injectable()
export class FuncionarioService {
  constructor(private http: HttpClient) { }
  url = 'https://localhost:44311/api/Funcionario';

  getAll() {
    return this.http.get<Funcionario[]>(this.url + '/' + 'GetAll');
  }

  getId(id: number) {
    return this.http.get<Funcionario>(this.url + '/Get/' + id);
  }

  post(funcionario: Funcionario) {
    return this.http.post(this.url + '/Post', funcionario);
  }

  update(funcionario: Funcionario) {
    return this.http.put(this.url + '/Update' , funcionario );
  }

  delete(id: number) {
    return this.http.delete(this.url + '/Delete/' + id);
  }
}
