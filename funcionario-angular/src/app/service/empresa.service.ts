import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empresa } from '../entities/empresa.entity';


@Injectable()
export class EmpresaService {
  constructor(private http: HttpClient) { }
  url = 'https://localhost:44311/api/Empresa';

  getAll() {
    return this.http.get<Empresa[]>(this.url + '/' + 'GetAll');
  }

  getId(id: number) {
    return this.http.get<Empresa>(this.url + '/Get/' + id);
  }

  post(empresa: Empresa) {
    return this.http.post(this.url + '/Post', empresa);
  }

  update(empresa: Empresa) {
    return this.http.put(this.url + '/Update' , empresa );
  }

  delete(id: number) {
    return this.http.delete(this.url + '/Delete/' + id);
  }
}
