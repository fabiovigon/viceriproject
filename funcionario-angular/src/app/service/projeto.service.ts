import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Projeto } from '../entities/projeto.entity';


@Injectable()
export class ProjetoService {
  constructor(private http: HttpClient) { }
  url = 'https://localhost:44311/api/Projeto';

  getAll() {
    return this.http.get<Projeto[]>(this.url + '/' + 'GetAll');
  }

  getId(id: number) {
    return this.http.get<Projeto>(this.url + '/Get/' + id);
  }

  post(projeto: Projeto) {
    return this.http.post(this.url + '/Post', projeto);
  }

  update(projeto: Projeto) {
    return this.http.put(this.url + '/Update' , projeto );
  }

  delete(id: number) {
    return this.http.delete(this.url + '/Delete/' + id);
  }
}
