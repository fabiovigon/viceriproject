import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from '../app/index/index.component';
import { FuncionarioAddComponent } from '../app/funcionario-add/funcionario-add.component';
import { FuncionarioEditComponent } from '../app/funcionario-edit/funcionario-edit.component';
import { ProjetoAddComponent } from './Projeto/projeto-add/projeto-add.component';
import { ClienteAddComponent } from './Cliente/cliente-add/cliente-add.component';
import { EmpresaAddComponent } from './empresa/empresa-add/empresa-add.component';
const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'index', component: IndexComponent},
  {path: 'add', component: FuncionarioAddComponent},
  {path: 'edit/:id', component: FuncionarioEditComponent},
  {path: 'addProjeto', component: ProjetoAddComponent},
  {path: 'addCliente', component: ClienteAddComponent},
  {path: 'addEmpresa', component: EmpresaAddComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
