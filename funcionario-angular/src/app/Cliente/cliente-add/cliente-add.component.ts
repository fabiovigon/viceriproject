import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteService } from '../../service/cliente.service';
import { ProjetoService } from '../../service/projeto.service';
import { Projeto } from '../../entities/projeto.entity';
import { Location } from '@angular/common';
@Component({
  selector: 'app-cliente-add',
  templateUrl: './cliente-add.component.html',
  styleUrls: ['./cliente-add.component.scss']
})
export class ClienteAddComponent implements OnInit {
  public projeto: Projeto[];
  clienteForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private projetoService: ProjetoService,
    private clienteService: ClienteService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.clienteForm = this.formBuilder.group({
      nomE_CLI: '',
      iD_PROJETO_CLIENTE: '',
      cnpj: '',
    });
   this.getProjeto();
  }

save() {
  this.clienteService.post(this.clienteForm.value).subscribe(
    res => {
      this.router.navigate(['']);
    },
    error => {
        alert(error);
    }
  );
}
  getProjeto() {
    this.projetoService.getAll()
    .subscribe(
      res => {
        this.projeto = res;
      },
      error => {
        alert(error);
      }
    );
  }
}
