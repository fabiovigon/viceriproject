import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FuncionarioService } from './service/funcionario.service';
import { IndexComponent } from './index/index.component';
import { FuncionarioEditComponent } from './funcionario-edit/funcionario-edit.component';
import { FuncionarioAddComponent } from './funcionario-add/funcionario-add.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjetoAddComponent } from './Projeto/projeto-add/projeto-add.component';
import { ProjetoService } from './service/projeto.service';
import { ClienteAddComponent } from './Cliente/cliente-add/cliente-add.component';
import { ClienteService } from './service/cliente.service';
import { EmpresaAddComponent } from './empresa/empresa-add/empresa-add.component';
import { EmpresaService } from './service/empresa.service';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    FuncionarioEditComponent,
    FuncionarioAddComponent,
    ProjetoAddComponent,
    ClienteAddComponent,
    EmpresaAddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    FuncionarioService,
    ProjetoService,
    ClienteService,
    EmpresaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
