import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FuncionarioService } from '../service/funcionario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Projeto } from '../entities/projeto.entity';
import { Funcionario } from '../entities/funcionario.entity';
import { ProjetoService } from '../service/projeto.service';
@Component({
  selector: 'app-funcionario-edit',
  templateUrl: './funcionario-edit.component.html',
  styleUrls: ['./funcionario-edit.component.scss']
})
export class FuncionarioEditComponent implements OnInit {
  funcionarioForm: FormGroup;
  projeto: Projeto[];
  funcionarios: Funcionario[];
  constructor(
    private formBuilder: FormBuilder,
    private funcionarioService: FuncionarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private projetoService: ProjetoService
  ) { }

  ngOnInit() {
    const id = this. activatedRoute.snapshot.params.id;
    this.funcionarioService.getId(id).subscribe(
      res => {
        this.funcionarioForm = this.formBuilder.group({
          id: res.id,
          nome_Func: res.nome_Func,
          cargo: res.cargo,
          cpf: res.cpf,
          iD_PROJETOS_FUNC: res.iD_PROJETOS_FUNC
        });


    },
      error => {
        console.log(error);
      }
    );
    this.getProjeto();
  }


  save() {
    this.funcionarioService.update(this.funcionarioForm.value).subscribe(
      res => {
        this.router.navigate(['']);
      },
      error => {
        console.log(error);
      }
    );
  }
  update(id: number) {
    this.router.navigate(['/edit/' + id]);
  }

  obterProjetos(iD_PROJETOS_FUNC: number): string {
    return this.projeto.find(projeto => projeto.id === iD_PROJETOS_FUNC).nomE_PROJETO;
  }

  getProjeto() {
    this.projetoService.getAll()
    .subscribe(
      res => {
        console.log(res);
        this.projeto = res;
      },
      error => {
        alert(error);
      }
    );
  }

}
