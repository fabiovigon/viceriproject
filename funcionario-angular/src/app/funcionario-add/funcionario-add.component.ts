import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FuncionarioService } from '../service/funcionario.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Funcionario } from '../entities/funcionario.entity';
import { Projeto } from '../entities/projeto.entity';
import { ProjetoService } from '../service/projeto.service';
@Component({
  selector: 'app-funcionario-add',
  templateUrl: './funcionario-add.component.html',
  styleUrls: ['./funcionario-add.component.scss']
})
export class FuncionarioAddComponent implements OnInit {
  public projeto: Projeto[];
  funcionarioForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private funcionarioService: FuncionarioService,
    private router: Router,
    private location: Location,
    private projetoService: ProjetoService
  ) { }

  ngOnInit() {
    this.funcionarioForm = this.formBuilder.group({

        nome_Func: '',
        cargo: '',
        CPF: '',
        iD_PROJETOS_FUNC: '',

    });
    this.getProjeto();
  }
    save() {

     this.funcionarioService.post(this.funcionarioForm.value).subscribe(
        res => {
          this.router.navigate(['']);
        },
        error => {
          console.log(error);
        }
      );
    }

    getProjeto() {
      this.projetoService.getAll()
      .subscribe(
        res => {
          console.log(res);
          this.projeto = res;
        },
        error => {
          alert(error);
        }
      );
    }
}

