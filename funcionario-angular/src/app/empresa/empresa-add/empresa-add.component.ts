import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmpresaService } from '../../service/empresa.service';
import { Router } from '@angular/router';
import { Funcionario } from '../../entities/funcionario.entity';
import { FuncionarioService } from '../../service/funcionario.service';
import { ClienteService } from '../../service/cliente.service';
import { Cliente } from '../../entities/cliente.entity';
import { Location } from '@angular/common';

@Component({
  selector: 'app-empresa-add',
  templateUrl: './empresa-add.component.html',
  styleUrls: ['./empresa-add.component.scss']
})
export class EmpresaAddComponent implements OnInit {
public funcionario: Funcionario[];
public cliente: Cliente[];
empresaForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private empresaService: EmpresaService,
    private funcionarioService: FuncionarioService,
    private clientService: ClienteService,
    private router: Router,
    private location: Location

  ) { }

  ngOnInit() {
    this.empresaForm =  this.formBuilder.group({
      nomE_EMPRESA: '',
      iD_CLI: '',
      iD_FUNC: '',
      cnpj: ''
    });
    this.getCliente();
    this.getFuncionario();
  }

  save() {
    this.empresaService.post(this.empresaForm.value).subscribe(
      res => {
        this.router.navigate(['']);
    },
    error => {
      alert(error);
    }
  );
  }

  getFuncionario() {
    this.funcionarioService.getAll()
    .subscribe(
      res => {
        this.funcionario = res;
      },
      error => {
        alert(error);
      }
    );
  }

  getCliente() {
    this.clientService.getAll()
    .subscribe(
      res => {
        this.cliente = res;
      },
        error => {
        alert(error);
      }
    );
  }

}
