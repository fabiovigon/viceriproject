import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProjetoService } from '../../service/projeto.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-projeto-add',
  templateUrl: './projeto-add.component.html',
  styleUrls: ['./projeto-add.component.scss']
})
export class ProjetoAddComponent implements OnInit {

  projetoForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private projetoService: ProjetoService,
    private router: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.projetoForm = this.formBuilder.group({
      NOME_PROJETO: ''
    });
  }
  save() {
    this.projetoService.post(this.projetoForm.value).subscribe(
      res => {
        this.router.navigate(['']);
      },
      error => {
        console.log(error);
      }
    );
  }

}
