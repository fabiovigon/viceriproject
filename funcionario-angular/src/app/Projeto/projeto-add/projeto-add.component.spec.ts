import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetoAddComponent } from './projeto-add.component';

describe('ProjetoAddComponent', () => {
  let component: ProjetoAddComponent;
  let fixture: ComponentFixture<ProjetoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjetoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
