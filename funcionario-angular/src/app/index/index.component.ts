import { Component, OnInit } from '@angular/core';
import { FuncionarioService } from '../service/funcionario.service';
import { Funcionario } from '../entities/funcionario.entity';
import { Router } from '@angular/router';
import { Projeto } from '../entities/projeto.entity';
import { ProjetoService } from '../service/projeto.service';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
   public funcionarios: Funcionario[];
   public projeto: Projeto[];

  constructor(
    private funcionarioService: FuncionarioService,
    private projetoService: ProjetoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadData();
    this.obterProjeto();
  }

  delete(id: number) {
    const result = confirm('Você tem certeza que deseja excluir esse Funcionário? Caso sim essa exclusão será permanente!');
    if (result) {
      this.funcionarioService.delete(id).subscribe(
        res => {
          this.loadData();
        },
        error => {
          alert(error);
        }
      );
    }
  }

  loadData() {

    this.funcionarioService.getAll().subscribe(
      res => {

        res.map(funcionario => {
          this.funcionarios = res;
          funcionario.nome_projeto = this.obterProjetos(funcionario.iD_PROJETOS_FUNC);
        });

      },
      error => {
        alert(error);
      }
    );
  }

  obterProjeto() {
    this.projetoService.getAll().subscribe(
        res => {
        this.projeto = res;
      },
      error => {
        alert(error);
      }
    );
  }
  update(id: number) {
    this.router.navigate(['/edit/' + id]);
  }

  obterProjetos(iD_PROJETOS_FUNC: number): string {
    return this.projeto.find(projeto => projeto.id === iD_PROJETOS_FUNC).nomE_PROJETO;
  }

}
