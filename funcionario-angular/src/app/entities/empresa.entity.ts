export class Empresa {
  id: number;
  nomE_EMPRESA: string;
  iD_CLI: number;
  iD_FUNC: number;
  cnpj: string;
}
